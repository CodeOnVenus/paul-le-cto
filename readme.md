# PAUL The CTO

a discord bot to learn computer concepts according to your level
[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](LICENSE)


![Logo](public/img/paulcto.jpg)

---

##  1. <a name='TableofContents'></a>Table of Contents

<!-- vscode-markdown-toc -->
* 1. [Table of Contents](#TableofContents)
* 2. [What is Paul the CTO ?](#Whatispaulcto)
* 3. [How to install the app ?](#Howtoinstalltheapp)
	* 3.1. [Prerequisites](#Prerequisites)
	* 3.2. [Clone it](#Cloneit)
	* 3.3. [Launch Docker](#LaunchDocker)
    * 3.4. [Composer & npm](#Composer&npm)
* 4. [Create an admin](#Createadmin)
* 5.[Configure your bot](#botconfig)
* 6.[Launch your local server with Symfony CLI](#server)


##  2. <a name='Whatispaulcto'></a>What is Paul the CTO ?

Paul the cto is a discord bot.
To use it is simple:
- Add PaulCTO#1804 on discord
- If you are new to computer science type "Junior"
- If you are confirmed in computer science, type "Senior"
- PaulCTO will answer you by teaching you a trick a concept or a definition depending on your selected level.

##  3. <a name='Howtoinstalltheapp'></a>How to install the app ?

PaulTheBot is a simple Symfony/PHP/PostgreSQL or Mysql application.

This documentation offers a simplified installation FOR DEVELOPMENT ONLY with Docker and symfony cli. You can do without it if you already have PostgreSQL or Mysql.

###  3.1. <a name='Prerequisites'></a>Prerequisites

- [PHP 8.0.23](https://www.php.net/downloads.php)
- [Composer](https://getcomposer.org/)
- [Docker](https://www.docker.com/)
- [ApiPlatform](https://api-platform.com) or
- [PostMan](https://www.postman.com)
- [Symfony CLI](https://symfony.com/download)
- [Webpack Encore](https://symfony.com/doc/current/frontend/encore/installation.html)
- [Initialise a Discord bot](https://discord.com/developers/applications)

###  3.2. <a name='Cloneit'></a>Clone it

```bash
git clone https://gitlab.com/MahaBcb.dev/paul-bot.git
cd paul-bot
```


###  3.3. <a name='LaunchDocker'></a>Lauch your database with docker

uncomment database url in .env

DATABASE_URL="mysql://127.0.0.1:51789/paul?sslmode=disable&charset=utf8mb4"

```bash
docker-compose up -d
```

###  3.4. <a name='Composer&npm'></a> Composer & npm

```bash
npm install
npm run build
composer install
php bin/console doctrine:schema:create
```
## 4. <a name='#Createadmin'></a>Create an admin

```bash
php bin/console app:create-admin admin@admin.com password
```
##  5.  <a name='botconfig'></a>Bot Config

Put your discord key of your bot in .env 
Put your API_URL (endpoint for api) in .env (for exemple: https://127.0.0.1:8000/api/articles/)

##  5.  <a name='server'></a>Launch your local server with Symfony CLI

```bash
symfony serve -d
```
And go to "https://localhost:8000"
authenticate yourself on the administration interface ("https://localhost:8000/admin") with:
    - email : admin@admin.com
    - password : password

ENJOYYYY <3