<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserManager
{
    public $encode;
    public $em;

    public function __construct(UserPasswordHasherInterface $encode, EntityManagerInterface $em)
    {
        $this->encode = $encode;
        $this->em = $em;
    }

    public function createUser(string $email, string $password): User
    {
        $user = new User();
        $user->setEmail($email)->setPassword($this->encodePassword($user, $password))->setRoles(['ROLE_ADMIN']);
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    public function encodePassword(User $user, string $password)
    {
        return $this->encode->hashPassword($user, $password);
    }
}
