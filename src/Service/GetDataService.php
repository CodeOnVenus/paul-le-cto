<?php

namespace App\Service;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Response;
use App\Repository\ArticleRepository;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GetDataService
{
    private $articleRepository;
    private $client;
    public $paramsBag;
    private $level;

    public function __construct(
        ArticleRepository $articleRepository,
        HttpClientInterface $client,
        ParameterBagInterface $paramsBag,

    ) {
        $this->client = $client;
        $this->articleRepository = $articleRepository;
        $this->paramsBag = $paramsBag;
    }

    //On récupere les article en fonction du niveau
    public function getArticlesResponse()
    {
        $articlesByLevel = $this->articleRepository->getRandomId($this->level);
        $randomId = array_rand($articlesByLevel, 1);
        $endpoint =  $this->paramsBag->get('API_URL') .  $randomId  ;
        $response = $this->client->request(
            'GET',
            $endpoint
        );
        return $response;
    }

    public function getContent() : ?string
    {
        $response = $this->getArticlesResponse()->toArray();
        return $response["title"] . " ?" . "\n" . $response["content"];

    }

    public function getLevel() : ?string
    {
        return $this->level;
    }

    public function setLevel(string $level) : self
    {
        $this->level = $level;

        return $this;
    }

}
