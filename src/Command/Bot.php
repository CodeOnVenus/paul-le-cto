<?php

namespace App\Command;

use Discord\Discord;
use Discord\WebSockets\Event;
use App\Service\GetDataService;
use Discord\Parts\Channel\Message;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Bot extends Command
{
    const JUNIOR = 'Junior';
    const SENIOR = 'Senior';

    protected static $defaultName = 'paul:post';
    public $getData;
    public $paramsBag;

    public function __construct(GetDataService $getData, ParameterBagInterface $paramsBag)
    {
        $this->getData = $getData;
        $this->paramsBag = $paramsBag;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $key = $this->paramsBag->get('KEY_DISCORD');
        $discord = new Discord([
            'token' => $key
                ]);
        $discord->on('ready', function (Discord $discord) {
            echo 'bot is ready';

            $discord->on(Event::MESSAGE_CREATE, function (Message $message, Discord $discord) {
                $content = $message->content;
                if ($content === self::JUNIOR) {

                    $this->getData->setLevel(self::JUNIOR);
                    $message->reply($message->member . 'Comme tu es junior, tu ne connais surement pas ' .
                        $this->getData->getContent());
                } elseif ($content === self::SENIOR) {

                    $this->getData->setLevel(self::SENIOR);
                    $message->reply($message->member . 'Comme tu es senior tu connais surement ' .
                    $this->getData->getContent());
                }
            });
        });

        $discord->run();
        return Command::SUCCESS;
    }
}
