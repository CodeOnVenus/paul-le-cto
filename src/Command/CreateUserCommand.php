<?php

namespace App\Command;

use App\Service\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:create-admin',
    description: 'Creates a new admin.',
    hidden: false,
    aliases: ['app:add-admin']
)]
class CreateUserCommand extends Command
{
    public function __construct(public UserManager $userManager)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('This command allows you to create a user...')
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The plain password of the user.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        $email = $input->getArgument('email');
        $password = $input->getArgument('password');

        $user = $this->userManager->createUser($email, $password);
        $output->writeln([
            'User created successfully!',
            '',
            'Email: ' . $user->getEmail(),
        ]);

        return Command::SUCCESS;
    }
}
