<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testUserIsTrue(): void
    {
        $user = new User();
        $user->setEmail('user@mail.fr')->setPassword('password');

        $this->assertTrue('user@mail.fr' === $user->getEmail());
        $this->assertTrue('password' === $user->getPassword());
    }
}
